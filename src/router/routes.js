import LandingPage from "@/views/LandingPage.vue";

const routes = [
    {
        path: "/",
        name: "landingpage",
        layout: "landingpage",
        component: LandingPage,
    },
];

export default routes;
