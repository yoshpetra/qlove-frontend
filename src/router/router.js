import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store/store.js";
import routes from "@/router/routes.js";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes.map((route) => ({
    path: route.path,
    name: route.name,
    component: route.component,
    beforeEnter(to, from, next) {
      store.dispatch("layoutModule/updateLayout", route.layout);
      next();
    },
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      } else {
        return { x: 0, y: 0 };
      }
    },
  })),
});

export default router;
