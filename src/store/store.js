import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import layoutModule from "@/store/global/updateLayout.js";

Vue.use(Vuex);
const vuexLocal = new VuexPersistence({
  key: "vuexOttoCommunity",
  storage: window.localStorage,
});

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { layoutModule },
  plugins: [vuexLocal.plugin],
});
