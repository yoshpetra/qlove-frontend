const layoutModule = {
    namespaced: true,
    state: {
        layout: "noLayout"
    },
    mutations: {
        updateLayout(state, layout) {
            state.layout = layout;
        }
    },
    actions: {
        updateLayout({ commit }, layout) {
            commit("updateLayout", layout);
        }
    }
};

export default layoutModule;
